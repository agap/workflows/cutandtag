#!/usr/bin/env python
import pandas as pd

import os,sys 

configfile:"config.yaml"
report: "report/workflow.rst"
genomes=config["reference"]

units = pd.read_csv("metadata.tsv", sep="\t", dtype = str).set_index(["sample","unit"], drop=False)
units.index.names = ["sample_id", "unit_id"]

index_columns = ["sample", "unit","fq1","fq2","control", 'text']
units = units.reindex(columns = index_columns)
replicat = units['sample'].value_counts() 



wildcard_constraints:
    sample = "|".join(units["sample"]),
    unit = "|".join(units["unit"]),
    reference = "|".join(genomes.keys())


def is_single_end(sample, unit):
    """Determine whether unit is single-end."""
    fq2_present = pd.isnull(units.loc[(sample, unit), "fq2"])
    return fq2_present

def get_fastq(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        return units.loc[(wildcards.sample, wildcards.unit), ["fq1"]].dropna()
    else:
        return units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()

def get_fastp_names(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        inFile = units.loc[(wildcards.sample, wildcards.unit) , ["fq1"]].dropna()
        return "--in1 " + inFile[0] + " --out1 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz"
    else:
        inFile = units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
        return "--in1 " + inFile[0] + " --in2 "+ inFile[1] + " --out1 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz --out2 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R2.trimmed.fastq.gz"


def get_fastp_files(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        return "results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz"
    else:
        return ("results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz","results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R2.trimmed.fastq.gz" )

def get_adapter_file(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        return "data/TruSeq3-SE.fa"
    else:
        return "data/TruSeq2-PE.fa"

def is_control(sample,unit):
    control = units.loc[(sample,unit)]["control"]
    return pd.isna(control) or pd.isnull(control)

def get_bowtie2_params(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        return "-1 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz"
    else:
        return "-1 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R1.trimmed.fastq.gz -2 results/trimming/" + wildcards.sample + "_" + wildcards.unit + "_R2.trimmed.fastq.gz"

def get_format(wildcards):
    """This function checks if the sample has paired end or single end reads and returns 1 or 2 names of the fastq files"""
    if is_single_end(wildcards.sample, wildcards.unit):
        return "BAM"
    else:
        return "BAMPE"

def get_multiqc_input(wildcards):
    multiqc_input = []
    for (sample, unit) in units.index:
        control_val = units.loc[(sample,unit)]["control"]
        multiqc_input.extend(
            expand (
                [
                    "results/{reference}/mapping/{sample}_{unit}.markdup.log",
                    "results/trimming/{sample}_{unit}.json"
                ],
                sample = sample,
                unit = unit,
                reference= genomes.keys()
            )
        )

        if sample != control_val:
            multiqc_input.extend(
                expand (
                    [
                        "results/{reference}/deeptools/{sample}_{unit}_{control}_fingerprint.tsv",
                        "results/{reference}/deeptools/{sample}.profile.png",
                        "results/{reference}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.xls",
                       #     "results/peak_calling_gopeaks/{sample}_{unit}_{control}_gopeaks.json"
                    ],
                    sample = sample,
                    unit = unit,
                    reference= genomes.keys(),
                    control = units.loc[sample]["control"]
                )
            )


    return multiqc_input



def all_input(wildcards):
    wanted_input = []
    wanted_input.extend(
        expand(
            [
                "results/{reference}/multiqc/multiqc.html"
            ],
            reference= genomes.keys()
        )
    )
 
    for (sample, unit) in units.index:
        control_val = units.loc[(sample,unit)]["control"]
        wanted_input.extend(
            expand (
                [  
                    "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
                    "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
                    "results/{reference}/coverage/individual/{sample}_{unit}.bw",
                    "results/{reference}/mapping/{sample}_{unit}_fragment_length.txt",
                    "results/{reference}/chipseeker/txdb.sqllite"
                ],
                sample = sample,
                unit = unit,
                reference= genomes.keys()
            )
        )
        if sample != control_val:
            wanted_input.extend(
                expand (
                    [  
                        "results/{reference}/peak_calling_macs2/{sample}.bed",
                        "results/{reference}/peak_calling_macs2/{sample}.bedgraph",
                        "results/{reference}/peak_calling_macs2/{sample}.bw",
                        "results/{reference}/coverage/compare/{sample}_{unit}_{control}.bw",
                        "results/{reference}/deeptools/{sample}.heatmap.png",
                        "results/{reference}/deeptools/{sample}.profile.png",
                        "results/{reference}/deeptools/{sample}.matrix.gz",
                        "results/{reference}/homer_macs2/{sample}_peak_annotation.txt",
#                                "results/{reference}/peak_calling_gopeaks/{sample}.bed",
 #                               "results/{reference}/peak_calling_gopeaks/{sample}.bedgraph",
  #                              "results/{reference}/peak_calling_gopeaks/{sample}.bw",
   #                            	"results/{reference}/homer_gopeaks/{sample}_peak_annotation.txt",
                        "results/{reference}/meme/{sample}/meme.txt",
                        "results/{reference}/meme/{sample}/fimo.tsv",
                        "results/{reference}/meme/{sample}/tomtom.tsv",
                        "results/{reference}/diffbind/{sample}_{unit}_{control}.csv"
                    ],
                    sample = sample,
                    unit = unit,
                    control = units.loc[(sample,unit)]["control"],
                    reference= genomes.keys()
                )
                )

    return wanted_input

onsuccess:
    from snakemake.report import auto_report
    auto_report(workflow.persistence.dag, "report/report.html")
    
    
rule all:
    input:
        all_input



rule gff2gtf:
    input:
        lambda wildcards: genomes[wildcards.reference]["gff3"]
    output:
        "results/{reference}/{reference}.gtf"
    singularity:
        config["containers"]["gffread"] 
    shell:"""
        gffread -E {input} -T -o {output}
    """

rule gff2bed:
    input:
        lambda wildcards: genomes[wildcards.reference]["gff3"]
    output:
        "results/{reference}/{reference}.bed"
    shell:"""
        scripts/gff2bed.pl {input} {output}
    """

rule genome_fasta:
    input:
        lambda wildcards: genomes[wildcards.reference]["assembly"]
    output:
        "results/{reference}/{reference}.fa"
    resources:
        partition='agap_short'
    shell:"""
        cp {input} {output}
    """

rule samtools_faidx:
    input:
        "results/{reference}/{reference}.fa"
    output:
        index= "results/{reference}/{reference}.fa.fai",
        size = "results/{reference}/{reference}.size"
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["samtools"]
    shell:"""
        samtools faidx {input};
        cut -f 1,2 {output.index} > {output.size}
    """



rule bowtie2_build:
    input:
        "results/{reference}/{reference}.fa"
    output:
        index= "results/{reference}/{reference}.fa.1.bt2"
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["bowtie2"] 
    shell:"""
        bowtie2-build --threads {params.threads} {input} {input}
    """

rule bwa_index:
    input:
        "results/{reference}/{reference}.fa"
    output:
        index= "results/{reference}/{reference}.fa.amb"
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["bwa-mem2"] 
    shell:"""
        bwa-mem2 index {input}
    """

rule make_txdb:
    input:
        gff3 = lambda wildcards: genomes[wildcards.reference]["gff3"]
    output:
        sqllite = "results/{reference}/chipseeker/txdb.sqllite"
    singularity:
        config["containers"]["chipseeker"]
    script:
        "./scripts/make_txdb.R"

rule fastp:
    input:
        get_fastq
    output:
        r1 = "results/trimming/{sample}_{unit}_R1.trimmed.fastq.gz",
        r2 = "results/trimming/{sample}_{unit}_R2.trimmed.fastq.gz",
        json = "results/trimming/{sample}_{unit}.json",
        html = "results/trimming/{sample}_{unit}_fastp.html"
    params:
        sample = "{sample}",
        min_length = config["fastp"]["min_length"],
        in_and_out_files =  get_fastp_names,
        adapter_file = get_adapter_file,
        threads = config["fastp"]["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["fastp"]
    shell:"""
        touch {output.r2};
        fastp {params.in_and_out_files} --thread {params.threads} --adapter_fasta {params.adapter_file} --length_required {params.min_length} -q 20 --json {output.json} --html {output.html}
    """
        
rule bowtie2:
    input:
        reads = get_fastp_files,
        index=rules.bowtie2_build.output,
        fasta = "results/{reference}/{reference}.fa"
    output:
        "results/mapping/{sample}_{unit}_bowtie.sam"
    params:
        reads = get_bowtie2_params,
        threads = config["threads"] * 2
    threads:
        config["threads"] * 2
    log:
        err="results/mapping/{sample}_{unit}_bowtie.log"
    singularity:
        config["containers"]["bowtie2"]  
    shell:"""
        bowtie2 --local --very-sensitive-local --no-unal --no-mixed --threads {params.threads} --no-discordant --phred33 -I 10 -X 700 -x {input.fasta} {params.reads} -S {output} 2>{log.err}  
    """

rule bwa_mem2:
    input:
        reads = get_fastp_files,
        index=rules.bwa_index.output,
        fasta = "results/{reference}/{reference}.fa"
    output:
        "results/{reference}/mapping/{sample}_{unit}_bwa.sam"
    params: 
        threads = config["threads"] * 2
    threads:
        config["threads"] * 2 
    singularity:
        config["containers"]["bwa-mem2"]  
    log:
        err="results/{reference}/mapping/{sample}_{unit}_bwa.log"
    shell:"""
        bwa-mem2 mem -t {params.threads} {input.fasta} {input.reads} > {output}    2>{log.err}  
    """

rule fragment_length:
    input:
        rules.bwa_mem2.output if config["mapping"] == 'bwa_mem2' else rules.bowtie2.output
    output:
        "results/{reference}/mapping/{sample}_{unit}_fragment_length.txt"
    singularity:
        config["containers"]["samtools"]
    shell:"""
        scripts/fragment_length.sh {input} {output}
    """
    
rule sambamba_view:
    input:
        rules.bwa_mem2.output if config["mapping"] == 'bwa_mem2' else rules.bowtie2.output
    output:
        "results/{reference}/mapping/{sample}_{unit}.bam"
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["sambamba"]
    shell:"""
        sambamba view -t {params.threads} -h -S -f bam -o {output} {input}

    """

# 
        #samtools view -@ {params.threads} -Sbh {input} > {output}

rule sambamba_sort:
    input:
        rules.sambamba_view.output
    output: 
        "results/{reference}/mapping/{sample}_{unit}.sort.bam"
    singularity:
        config["containers"]["sambamba"]
    params:
        threads = config["threads"],
        directory = "results/{reference}/mapping"
    threads:
        config["threads"]
    log:
        "results/{reference}/mapping/{sample}_{unit}.sort.log"
    shell:"""
        sambamba sort --tmpdir={params.directory} -t {params.threads} -o {output} {input} > {log} 2>&1
    """

rule sambamba_markdup:
    input:
        rules.sambamba_sort.output
    output:
        "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam"
    singularity:
        config["containers"]["sambamba"]
    params:
        threads = config["threads"],
        directory = "results/{reference}/mapping"
    threads:
        config["threads"]
    log:
        "results/{reference}/mapping/{sample}_{unit}.markdup.log"
    shell:"""
        sambamba markdup --tmpdir={params.directory} -t {params.threads} {input} {output} > {log} 2>&1
    """



rule sambamba_index:
    input:
        rules.sambamba_markdup.output
    output:
        "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai"
    singularity:
        config["containers"]["sambamba"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    log:
        "results/{reference}/mapping/{sample}_{unit}.index.log"
    shell:"""
        sambamba index -t {params.threads} {input} > {log} 2>&1
    """

rule bamCoverage:
    input:
        bam = rules.sambamba_markdup.output,
        index =rules.sambamba_index.output
    output:
        "results/{reference}/coverage/individual/{sample}_{unit}.bw"
    singularity:
        config["containers"]["deeptools"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    shell:"""        
        bamCoverage -b {input.bam} -o {output} -p {params.threads} --binSize 10 --smoothLength 50 --normalizeUsing CPM
    """



rule bamCompare:
    input:     
        sample =  "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
        sample_index = "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
        control ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam",
        control_index ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam.bai"
    output:
        bigwig = "results/{reference}/coverage/compare/{sample}_{unit}_{control}.bw"
    threads:
        config["threads"]
    singularity:
        config["containers"]["deeptools"]
    params:
        labels = "{sample} {unit} {control}",
        threads = config["threads"]
    shell:"""
        bamCompare -b1 {input.sample} -b2 {input.control} --numberOfProcessors {params.threads}   --binSize 20 --smoothLength 60 -of bigwig -o {output.bigwig}
    """

rule plotFingerprint:
    input:     
        sample =  "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
        sample_index = "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
        control ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam",
        control_index ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam.bai"
    output:
        tsv = "results/{reference}/deeptools/{sample}_{unit}_{control}_fingerprint.tsv",
        metrics =  "results/{reference}/deeptools/{sample}_{unit}_{control}_fingerprint.txt",
        png = report(
                "results/{reference}/deeptools/{sample}_{unit}_{control}_fingerprint.png",
                caption="report/fingerprint.rst",
                category="Fingerprint {reference}"
            )
    threads:
        config["threads"]
    singularity:
        config["containers"]["deeptools"]
    params:
        labels = "{sample} {control}",
        threads = config["threads"]
    shell:"""
        plotFingerprint -b {input.sample} {input.control} --numberOfProcessors {params.threads} --minMappingQuality 30 -T 'Fingerprints ' --labels {params.labels} --plotFile {output.png} --outRawCounts {output.tsv} --outQualityMetrics {output.metrics} 
    """


    

rule computeMatrix:
    input:
        bed = rules.gff2bed.output,
        sample = lambda w:  list(set(
            expand( "results/{{reference}}/coverage/compare/{sample}_{unit}_{control}.bw",
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample].control.to_list()
            )
        )) 
    output:
        "results/{reference}/deeptools/{sample}.matrix.gz"
    singularity:
        config["containers"]["deeptools"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    shell:"""        
        computeMatrix reference-point --referencePoint TSS -b 3000 -a 3000 -R {input.bed} --numberOfProcessors {params.threads} -S {input.sample}  --skipZeros --binSize 20  -o {output}
    """
     

rule plotProfile:   
    input: 
        bigwig =rules.computeMatrix.output
    output:
        report = report(
               "results/{reference}/deeptools/{sample}.profile.png",
                caption="report/profile.rst",
                category="Profile {reference}"
            )
    singularity:
        config["containers"]["deeptools"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    shell:""" 
        plotProfile -m {input} --numPlotsPerRow 2  -out {output}
    """

rule plotHeatmap:
    input: 
        bigwig =rules.computeMatrix.output
    output:
        report = report(
                "results/{reference}/deeptools/{sample}.heatmap.png",
                caption="report/heatmap.rst",
                category="Heatmap {reference}"
            )
        
    singularity:
        config["containers"]["deeptools"]
    params:
        threads = config["threads"]
    threads:
        config["threads"]
    shell:""" 
        plotHeatmap -m {input} -out {output}
    """
 
rule gopeaks:
    input:
        sample =  "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
        sample_index = "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
        control ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam",
        control_index ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam.bai"
    output:
        bed = "results/{reference}/peak_calling_gopeaks/{sample}_{unit}_{control}_peaks.bed",
        json = "results/{reference}/peak_calling_gopeaks/{sample}_{unit}_{control}_gopeaks.json"
    log:
        "results/{reference}/peak_calling_gopeaks/{sample}_{unit}_{control}.log"
    params:
        advanced_parameters = config["gopeaks"]["advanced_parameters"],
        prefix =  "results/{reference}/peak_calling_gopeaks/{sample}_{unit}_{control}"
    singularity:
        config["containers"]["gopeaks"]
    shell:"""
        gopeaks --bam {input.sample} --control {input.control} {params.advanced_parameters}  -o {params.prefix} > {log} 2>&1;
    """


rule macs2:
    input:
        sample =  "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
        sample_index = "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
        control ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam",
        control_index ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam.bai"
    output:
        bed = "results/{reference}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.narrowPeak",
        xls = "results/{reference}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.xls"
    log:
        "results/{reference}/peak_calling_macs2/{sample}_{unit}_{control}.log"
    params:
        advanced_parameter = config["macs2"]["advanced_parameter"],
        is_single = get_format,
        outdir = "results/{reference}/peak_calling_macs2",
        name = "{sample}_{unit}_{control}",
        genome_size =  config["genome_size"]
    singularity:
        config["containers"]["macs2"]
    shell:"""
         macs2 callpeak -t {input.sample} -c {input.control} -f {params.is_single} -n {params.name} -g {params.genome_size} {params.advanced_parameter} --outdir {params.outdir}  2> {log};
    """


rule diffbind_write_csv:
    input:
        sample =  "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam",
        sample_index = "results/{reference}/mapping/{sample}_{unit}.sorted.markd.bam.bai",
        control ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam",
        control_index ="results/{reference}/mapping/{control}_{unit}.sorted.markd.bam.bai",
        narrow = "results/{reference}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.narrowPeak",
    output:
        "results/{reference}/diffbind/{sample}_{unit}_{control}.csv" 
    run:
        with open(output[0], 'w') as f:
            f.write(wildcards.sample +"_"+wildcards.unit +"," + wildcards.sample +"," + wildcards.sample +"," + wildcards.sample +"," + wildcards.sample +"," + wildcards.unit+","+input[0]+","+input[2]+","+wildcards.control+","+input[4]+",narrow\n")
          
 
 

rule chipseeker:
    input:
        peak = lambda w:  list(set(
            expand("results/{{reference}}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.narrowPeak",
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample].control.to_list()
            )
        )),
        sqllite = rules.make_txdb.output.sqllite
    output:
        plot_average = report(
                "results/{reference}/chipseeker/{sample}_plot_average.pdf",
                caption="report/plot_average.rst",
                category="ChipSeeker (Plot profile {reference})"
            ),
        plot_average_confidence = report(
                "results/{reference}/chipseeker/{sample}_plot_confidence.pdf",
                caption="report/plot_average_confidence.rst",
                category="ChipSeeker (Plot confidence profile {reference}))"
            ), 
        plot_annotbar  = report(
                "results/{reference}/chipseeker/{sample}_annobar.pdf",
                caption="report/plot_annotbar.rst",
                category="ChipSeeker (Annotation {reference}))"
            ),
        plot_disttotss  = report(
                "results/{reference}/chipseeker/{sample}_plot_disttotss.pdf",
                caption="report/plot_disttotss.rst",
                category="ChipSeeker (Distance to TSS {reference}))"
            ) 
    params:
        directory = "results/{reference}/peak_calling_macs2/"
    singularity:
        config["containers"]["chipseeker"]
    script:
        "./scripts/chipseeker.R"



rule bedtools_multiinter_macs2:
    input:
        lambda w:  list(set(
            expand("results/{{reference}}/peak_calling_macs2/{sample}_{unit}_{control}_peaks.narrowPeak",
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample].control.to_list() 
            )
        ))
    output:
        "results/{reference}/peak_calling_macs2/{sample}.bed"
    run:
        if replicat[wildcards.sample] == 1:
            shell("cp {input} {output}")
        else:
            use_singularity = True
            singularity_container = config["containers"]["bedtools"]
            # Generate the field list based on the number of input files
            field_list = ','.join([str(i) for i in range(1, len(input) + 1)])

            # Use the generated field list in the bedtools multiinter command

            shell("set +euo pipefail && singularity exec {singularity_container} bedtools multiinter -cluster -i {input} | grep '{field_list}' > {output} && set -euo pipefail")




rule bedtools_multiinter_gopeaks:
    input:
        lambda w:  list(set(
            expand("results/{{reference}}/peak_calling_gopeaks/{sample}_{unit}_{control}_peaks.bed",
                sample = w.sample,
                unit = units.loc[units['sample'] == w.sample].unit.to_list(),
                control = units.loc[units['sample'] == w.sample].control.to_list() 
            )
        ))
    output:
        "results/{reference}/peak_calling_gopeaks/{sample}.bed"
    run:
        if replicat[wildcards.sample] == 1:
            shell("cp {input} {output}")
        else:
            use_singularity = True
            singularity_container = config["containers"]["bedtools"]
            # Generate the field list based on the number of input files
            field_list = ','.join([str(i) for i in range(1, len(input) + 1)])
            
            # Use the generated field list in the bedtools multiinter command
            shell("set +euo pipefail && singularity exec {singularity_container} bedtools multiinter -i {input} | grep '{field_list}' > {output} && set -euo pipefail")

rule annotate_macs2:
    input:
        peaks = "results/{reference}/peak_calling_macs2/{sample}.bed",
        fasta =  "results/{reference}/{reference}.fa",
        gtf   = rules.gff2gtf.output
    output:
        "results/{reference}/homer_macs2/{sample}_peak_annotation.txt"
    singularity:
        config["containers"]["homer"]
    params:
        threads = config["threads"]
    shell:"""
        annotatePeaks.pl {input.peaks} {input.fasta} -gtf {input.gtf} > {output}
    """

rule annotate_gopeaks:
    input:
        peaks =  "results/{reference}/peak_calling_gopeaks/{sample}.bed",
        fasta =  "results/{reference}/{reference}.fa",
        gtf   = rules.gff2gtf.output
    output:
        "results/{reference}/homer_gopeaks/{sample}_peak_annotation.txt"
    singularity:
        config["containers"]["homer"]
    params:
        threads = config["threads"]
    shell:"""
        annotatePeaks.pl {input.peaks} {input.fasta} -gtf {input.gtf} > {output}
    """

rule bed2fasta:
    input:
        bed = "results/{reference}/peak_calling_macs2/{sample}.bed",
        fasta =  "results/{reference}/{reference}.fa"
    output:
        fasta = "results/{reference}/peak_calling_macs2/{sample}.fa"
    singularity:
        config["containers"]["bedtools"]
    shell:
        "bedtools getfasta -fi {input.fasta} -bed {input.bed} -fo {output.fasta}"

rule seqtk_filter_fasta:
    input:
        "results/{reference}/peak_calling_macs2/{sample}.fa"
    output:
        "results/{reference}/peak_calling_macs2/{sample}_trimmed.fa"
    singularity:
        config["containers"]["seqtk"]
    shell:
        "seqtk seq -L 20 {input} > {output}"

rule motif_discovery:
    input:
        rules.seqtk_filter_fasta.output
    output:
        txt = "results/{reference}/meme/{sample}/meme.txt",
        report = report(
                "results/{reference}/meme/{sample}/meme.html",
                caption="report/meme.rst",
                category="Motif Discovery (meme {reference})"
            )       
    singularity:
        config["containers"]["meme"]
    params:
        prefix = "results/{reference}/meme/{sample}",
        min_length = config["meme"]["min_length"],
        max_length = config["meme"]["max_length"] 
    shell:"""
        meme {input} -minw {params.min_length} -maxw {params.max_length} -dna -mod oops -nostatus -oc {params.prefix}  
    """

rule motif_scanning:
    input:
        motif = rules.motif_discovery.output.txt,
        reference =  "results/{reference}/{reference}.fa"
    output:
        tsv = "results/{reference}/meme/{sample}/fimo.tsv",
        report = report(
                "results/{reference}/meme/{sample}/fimo.html",
                caption="report/fimo.rst",
                category="Motif Scanning (fimo {reference})"
            )
    singularity:
        config["containers"]["meme"]
    params:
        prefix = "results/{reference}/meme/{sample}",
        verbosity = config["meme_fimo"]["verbosity"],
        threshold = config["meme_fimo"]["threshold"]
    log:
        "results/{reference}/meme/{sample}_motifscanning.log"
    shell:"""
        fimo -verbosity {params.verbosity} --thresh {params.threshold} -oc {params.prefix} {input.motif} {input.reference} &>{log}
    """

rule motif_search:
    input:
        motif = rules.motif_discovery.output.txt,
        db = config["jaspar_db"]
    output:
        tsv = "results/{reference}/meme/{sample}/tomtom.tsv",
        report = report(
                "results/{reference}/meme/{sample}/tomtom.html",
                caption="report/tomtom.rst",
                category="Motif Searching (tomtom {reference})"
            )
    singularity:
        config["containers"]["meme"]
    params:
        directory = "results/{reference}/meme/{sample}" 
    shell:"""
        tomtom -oc {params.directory} {input.motif} {input.db} 
    """


rule bedgraph_gopeaks:
    input:
        peaks = "results/{reference}/peak_calling_gopeaks/{sample}.bed",
        genome_size = rules.samtools_faidx.output.size
    output:
        bedgraph = "results/{reference}/peak_calling_gopeaks/{sample}.bedgraph",
        bigwig = "results/{reference}/peak_calling_gopeaks/{sample}.bw"
    singularity:
        config["containers"]["bedgraphtobigwig"]
    params: 
        threads = config["threads"]
    shell:"""
        scripts/bed2bigwig.sh {input.peaks} {output.bedgraph};
        bedGraphToBigWig  {output.bedgraph} {input.genome_size}  {output.bigwig};
    """

rule bedgraph_macs2:
    input:
        peaks = "results/{reference}/peak_calling_macs2/{sample}.bed",
        genome_size = rules.samtools_faidx.output.size
    output:
        bedgraph = "results/{reference}/peak_calling_macs2/{sample}.bedgraph",
        bigwig = "results/{reference}/peak_calling_macs2/{sample}.bw"
    singularity:
        config["containers"]["bedgraphtobigwig"]
    params:
        threads = config["threads"]
    shell:"""
        scripts/bed2bigwig.sh {input.peaks} {output.bedgraph};
        bedGraphToBigWig  {output.bedgraph} {input.genome_size}  {output.bigwig};
    """



rule multiqc:
    input:
        get_multiqc_input
    output:
        "results/{reference}/multiqc/multiqc.html"
    singularity:
        config["containers"]["multiqc"]
    log:
        "results/{reference}/multiqc.log"
    params:
        directory = "results/{reference}/multiqc",
        name = "multiqc.html"
    shell:
        "multiqc --force -o {params.directory} -n {params.name} {input} &> {log}"
        
 
    
    
