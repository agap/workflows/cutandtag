#!/bin/bash
#samtools view -F 0x04 $1 | awk -F'\t' 'function abs(x){return ((x < 0.0) ? -x : x)} {print abs($9)}' | sort | uniq -c | awk -v OFS="\t" '{print $2, $1/2}' > $2

samtools view $1 | awk '$9>0' | cut -f 9 | sort | uniq -c | sort -b -k2,2n | sed -e 's/^[ \t]*//' | sed 's/ /\t/g' | awk -v OFS='\t' '{print $2,$1}' > $2