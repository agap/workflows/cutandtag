
library(DiffBind) 
library(rtracklayer)
library(dplyr)
# Read a csv file
# Reading in the peaksets

args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
}  
samples <- read.csv(args[1])

# Look at the loaded metadata
names(samples)

# Create a DBA object
object <- dba(sampleSheet=samples)

# Plot correlation heatmap
dba.plotHeatmap(object)

# Counting reads for every sample

object.counted <- dba.count(object, bUseSummarizeOverlaps=TRUE)
object.counted


# Plot PCA
dba.plotPCA(object.counted,  attributes=DBA_FACTOR, label=DBA_ID)
info<-dba.show(object.counted)
info
info$FRiP
libsizes <-cbind(LibReads=info$Reads,FRiP=info$FRiP,PeakReads=round(info$Reads *info$FRiP))
rownames(libsizes)<-info$ID
libsizes



# Plot correlation heatmap based on the count scores.

dba.plotHeatmap(object.counted)


# Normalizing the data
object.normalize<-dba.normalize(object.counted)
object.normalize 

# Plot PCA 
dba.plotPCA(object.normalize,  attributes=DBA_FACTOR, label=DBA_ID)
 

# Establishing a model design and contrast
 
object.contrast <- dba.contrast(object.normalize, categories=DBA_FACTOR)
object.contrast 

# Performing the differential analysis
 
object.analysed <- dba.analyze(object.contrast, method=DBA_ALL_METHODS)
dba.show(object.analysed, bContrasts=T)
contrast<-dba.show(object.analysed, bContrasts=T)
contrast
dba.plotPCA(object.analysed,attributes=c(DBA_TISSUE,DBA_CONDITION),label=DBA_REPLICATE)
 


# Retrieving the differentially bound sites
 
for (i in 1:nrow(contrast)){
   
  prefix1 <-paste(contrast[i,"Group"], "_vs_",contrast[i,"Group2"],sep='')
  prefix2 <-paste(contrast[i,"Group2"], "_vs_",contrast[i,"Group"],sep='')

  plot(object.analysed,contrast=i) 
  dba.plotVenn(object.analysed,contrast=i,bDB=TRUE, bGain=TRUE,bLoss=TRUE,bAll=FALSE) 
  dba.plotVenn(object.analysed,contrast=i,method=DBA_ALL_METHODS)
  dba.plotHeatmap(object.analysed, contrast=i)
  dba.plotHeatmap(object.analysed, ColAttributes = DBA_FACTOR, contrast=i, correlations=FALSE)
  dba.plotVolcano(object.analysed, contrast=i)
  dba.plotPCA(object.analysed, contrast = i)
  
  dba.plotBox(object.analysed,contrast=i)
  report <- dba.report(object.analysed,contrast=i)
  report.df <- as.data.frame(report)
  write.table(report.df, paste(prefix1,"_report.csv",sep=''), sep="\t", quote=F, row.names=F)
  first_enrich <- report.df %>% 
    filter(FDR < 0.05 & Fold > 0) %>% 
    select(seqnames, start, end)
  write.table(first_enrich, file=paste(prefix1,"_enriched.bed",sep=''), sep="\t", quote=F, row.names=F, col.names=F)
  
  second_enrich <- report.df %>% 
    filter(FDR < 0.05 & Fold < 0) %>% 
    select(seqnames, start, end)
  write.table(second_enrich, file=paste(prefix2,"_enriched.bed",sep=''), sep="\t", quote=F, row.names=F, col.names=F)
  
}  
 