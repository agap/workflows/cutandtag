#!/usr/bin/perl
use File::Basename;use POSIX;
my $directory = shift;
my $technology = shift;

open(IN,"ls $directory/*gz|");
my %igg;
my %sample;
while(<IN>){
    chomp;
    my ($name,$path,$suffix) = fileparse($_,".fastq.gz");
    my ($id,$read,$other) = (split(/\_/,$name));

    if ($technology eq "cutandtag"){

        my @data = (split(/\-/,$id));
        my $uniq = join("-",$data[0],$data[1],$data[2]);
        if (scalar(@data) == 5) {
            if (isdigit($data[-1]) ) {
                pop @data;
                $id = join("-",@data);
            }
            else {
                my $last = pop @data;
                $id = join("-", @data). $last;
            }
            
        }
        if ($data[3] eq "IgG") {
            $igg{$uniq} = $id;
        } 
        push @{$sample{$id}{$read}} , $_;
    }
    else {
        my ($id,$read) = (split(/\_/,$name));
        
        push @{$sample{$id}{$read}} , $_;

    }
 
}
close IN;
print join("\t","sample","unit","fq1","fq2","control"),"\n";
foreach my $id (sort keys %sample) { 

    my @data = (split(/\-/,$id));
    my $uniq = join("-",$data[0],$data[1],$data[2]); 
    my @file = @{$sample{$id}{"1"}};
    for (my $i = 0;$i<=$#file;$i++)  { 
        print join("\t",$id,$i+1,$sample{$id}{"1"}[$i],$sample{$id}{"2"}[$i],$igg{$uniq}) ,"\n";
    } 

}
 