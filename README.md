# Snakemake workflow: CUT&Tag or DAP-Seq data analysis

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)    

**Table of Contents** 

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Prerequisites](#prerequisites)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)
  - [Results](#results)

## Objective

CUT&Tag or DAP-Seq data analysis

## Dependencies

* fastp: A tool designed to provide fast all-in-one preprocessing for FastQ files. (https://github.com/OpenGene/fastp)
* bowtie2 : An ultrafast and memory-efficient tool for aligning sequencing reads to long reference sequences (https://github.com/BenLangmead/bowtie2)
* sambamba markdup : finding duplicate reads in BAM file (https://lomereiter.github.io/sambamba/)
* samtools : Utilities for the Sequence Alignment/Map (SAM) format (http://www.htslib.org/doc/samtools.html)
* deeptools : Tools for exploring deep sequencing data. (https://deeptools.readthedocs.io/en/develop/index.html)
    - multiBamSummary
    - bamCoverage
    - bamCompare
    - computeMatrix
    - plotFingerprint
    - plotPCA
    - plotHeatmap
* MACS2 callpeak :Function to call peaks from alignment results (https://macs3-project.github.io/MACS/)
* Homer : A suite of tools for Motif Discovery and next-gen sequencing analysis. (http://homer.ucsd.edu/homer/index.html)
* ChipSeeker: ChIPseeker for ChIP peak Annotation, Comparison, and Visualization (https://bioconductor.org/packages/release/bioc/html/ChIPseeker.html)
* Meme suite: Motif-based sequence analysis tools (https://meme-suite.org/meme/)
    - meme : Multiple Em for Motif Elicitation
    - fimo : Find Individual Motif Occurences
    - tomtom : Motif comparison tools
* Diffbind: Differential binding analysis of ChIP-Seq peak data (https://bioconductor.org/packages/release/bioc/html/DiffBind.html)


## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+ 

## Overview of programmed workflow

<p align="center">
<img src="images/rulegraph.svg">
</p>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:

```bash
  git clone https://gitlab.cirad.fr/agap/workflows/cutandtag.git
```


- Change directories into the cloned repository:

```bash
  cd cutandtag
```

- Edit the configuration file config.yaml and complete the section reference with one or more reference, with assembly (fasta) and annotation (gff3)

```bash
reference: 
  "IR64":
    "assembly": "/storage/replicated/cirad/web/HUBs/rice2/Oryza_sativa_indica_1B1_IR64/Oryza_sativa_indica_1B1_IR64.assembly.fna"
    "gff3": "/storage/replicated/cirad/web/HUBs/rice2/Oryza_sativa_indica_1B1_IR64/Oryza_sativa_indica_1B1_IR64.gff3"
  "Azucena":
    "assembly": "/storage/replicated/cirad/web/HUBs/rice/Oryza_sativa_japonica_trop1_azucena/Oryza_sativa_japonica_trop1_azucena.assembly.fna"
    "gff3": "/storage/replicated/cirad/web/HUBs/rice/Oryza_sativa_japonica_trop1_azucena/Oryza_sativa_japonica_trop1_azucena.gff3"


genome_size: "3.6e+8" 
threads: 4

macs2:
  advanced_parameter: " --nomodel --extsize 100 --fe-cutoff 1.5 -q 0.05 --keep-dup auto "
  memory: "16G"

```


- Edit the configuration file profile/config.yaml for SLURM integration

  * partition

  * mem_mb


- Create a file metadata.tsv
 
|sample  |unit  |fq1  |fq2  |control|
|--|--|--|--|--|
|CT-AZU-TS-IgG  |1  |/path/to/fastq/CT-AZU-TS-IgG-1/R1  |/path/to/fastq/CT-AZU-TS-IgG-1/R2  |CT-AZU-TS-IgG  |
|CT-AZU-TS-IgG  |2  |/path/to/fastq/CT-AZU-TS-IgG-2/R1  |/path/to/fastq/CT-AZU-TS-IgG-2/R2  |CT-AZU-TS-IgG  |
|CT-AZU-TS-K27  |1  |/path/to/fastq/CT-AZU-TS-K27-1/R1  |/path/to/fastq/CT-AZU-TS-K27-1/R2  |CT-AZU-TS-IgG  |
|CT-AZU-TS-K27  |2  |/path/to/fastq/CT-AZU-TS-K27-2/R1  |/path/to/fastq/CT-AZU-TS-K27-2/R2  |CT-AZU-TS-IgG  |
|CT-AZU-TS-K4  |1  |/path/to/fastq/CT-AZU-TS-K4-1/R1  |/path/to/fastq/CT-AZU-TS-K4/R2  |CT-AZU-TS-IgG  |
|CT-AZU-TS-K4  |2  |/path/to/fastq/CT-AZU-TS-K4-2/R1  |/path/to/fastq/CT-AZU-TS-K4/R2  |CT-AZU-TS-IgG  |
 
 For this, an example of scripts is provided (scripts/create_metadata.pl) to generate this file. You can modify this according to your needs
 
 ```bash
 scripts/create_metadata.pl /storage/replicated/cirad/projects/LANDSREC/raw_data/CUTandTag/BHKGMGDRX3/ > metadata.tsv
 ```
 
- Print shell commands to validate your modification (mode dry-run)

```bash
sbatch snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```

- Unlock directory

```bash
sbatch snakemake.sh unlock
```

- Generate DAG file

```bash
sbatch snakemake.sh dag
```

## Results

- results/*reference*/peak_calling_macs2/ 
  - <sample>_<replicate>_input_peaks.narrowPeak : files containing the peak intervals identified by the macs2. This file can be loaded on IGV.
  - <sample>.bed :  BED files containing the consensus peak intervals per sample
  - <sample>.fa : Consensus peak as fasta

- results/*reference*/deeptools/

- results/*reference*/coverage/indiviual : Coverage tracks: these files are in bigwig format, a compact representation of the genome coverage of each sample and can be used in genome browsers. This file can be loaded on IGV.

- results/*reference*/coverage/compare : Coverage tracks: these files are in bigwig format, a compact representation of the genome coverage of each sample compare to control and can be used in genome browsers. This file can be loaded on IGV.

- results/*reference*/homer_macs2/
  - <sample>_peak_annotation.txt

- report/report.html : Combine all results

## Visualize result using IGV

Download IGV : https://igv.org/

## Differential Binding Site

```bash
cat data/header_diffbind.csv results/diffbind/* > diffbind.csv
singularity exec https://depot.galaxyproject.org/singularity/bioconductor-diffbind%3A3.2.7--r41h399db7b_0 Rscript --vanilla scripts/diffbind.R diffbind.csv
```

